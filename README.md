# Pinniped authenticator

A friendly UI for developers so they can easily access multiple clusters

<img src="https://gitlab.com/commonground/haven/pinniped-authenticator/uploads/ff6f730b9bceb708f6b6295157d01ea9/image.png" alt="Screenshot of Pinniped authenticator" width="600"/>

