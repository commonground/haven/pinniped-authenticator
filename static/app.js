const data = document.querySelector('#cluster-list-data')
const clusters = JSON.parse(data.innerHTML)

const ClusterList = {
  template: `
    <div class="cluster-list">
      <label v-for="cluster in clusters" class="cluster cursor-pointer" :class="{selected: modelValue === cluster.name}">
        <input type="radio" name="cluster" :value="cluster.name" :checked="modelValue === cluster.name" @input="updateSelectedCluster" class="sr-only" />
        <div>{{ cluster.name }}</div>
        <div class="cluster__description">{{ cluster.description }}</div>
      </label>
    </div>`,
  data() {
    return {
      clusters: clusters,
    }
  },
  methods: {
    updateSelectedCluster(e) {
      this.$emit('update:modelValue', e.target.value)
    }
  },
  props: {
    modelValue: String
  }
}

const ConfigurationCommands = {
  template: `
    <p>
    <h2>Run configuration commands</h2>
    These commands will update your Kubernetes configuration file
    <pre @click="selectText($event.target)" class="cursor-pointer" tabindex="0"><code>kubectl config set clusters.{{ cluster.name }}.server {{ cluster.concierge.endpoint }}
kubectl config set clusters.{{ cluster.name }}.certificate-authority-data {{ cluster.concierge.caBundleData }}</code></pre>
    <pre @click="selectText($event.target)" class="cursor-pointer" tabindex="0"><code>kubectl config set-credentials pinniped-{{ cluster.name }} \\
  --exec-api-version="client.authentication.k8s.io/v1beta1" \\
  --exec-command=pinniped \\
  --exec-arg="login" \\
  --exec-arg="oidc" \\
  --exec-arg="--enable-concierge" \\
  --exec-arg="--concierge-authenticator-name={{ cluster.concierge.authenticatorName }}" \\
  --exec-arg="--concierge-authenticator-type={{ cluster.concierge.authenticatorType }}" \\
  --exec-arg="--concierge-endpoint={{ cluster.concierge.endpoint }}" \\
  --exec-arg="--concierge-ca-bundle-data={{ cluster.concierge.caBundleData }}" \\
  --exec-arg="--issuer={{ cluster.issuer }}" \\
  --exec-arg="--request-audience={{ cluster.requestAudience }}" \\
  --exec-arg="--upstream-identity-provider-name={{ cluster.upstreamIdentityProvider.name }}"</code></pre>

    <pre @click="selectText($event.target)" class="cursor-pointer" tabindex="0"><code>kubectl config set-context pinniped-{{ cluster.name }} \\
  --cluster={{ cluster.name }} \\
  --user=pinniped-{{ cluster.name }}</code></pre>

    <pre @click="selectText($event.target)" class="cursor-pointer" tabindex="0"><code>kubectl config use-context pinniped-{{ cluster.name }}</code></pre>
  </p>
  `,
  props: {
    cluster: Object
  },
  methods: {
    selectText(element) {
      if (document.selection) { // IE
          var range = document.body.createTextRange();
          range.moveToElementText(element);
          range.select();
      } else if (window.getSelection) {
          var range = document.createRange();
          range.selectNode(element);
          window.getSelection().removeAllRanges();
          window.getSelection().addRange(range);
      }
    }
  }
}

const App = {
  template: `
    <div>
      <ClusterList v-model="selectedClusterName" />
      <ConfigurationCommands :cluster="selectedCluster" />
    </div>
  `,
  components: {
    ClusterList,
    ConfigurationCommands
  },
  data() {
    return {
      selectedClusterName: clusters.length > 0 ? clusters[0].name : null
    }
  },
  computed: {
    selectedCluster: function() {
      return clusters.find(cluster => cluster.name === this.selectedClusterName)
    }
  }
}

Vue.createApp(App).mount('#app')
